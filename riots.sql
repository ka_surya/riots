-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2017 at 02:53 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `riots`
--
CREATE DATABASE IF NOT EXISTS `riots` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `riots`;

-- --------------------------------------------------------

--
-- Table structure for table `cartproducts`
--

CREATE TABLE `cartproducts` (
  `CartID` int(11) NOT NULL,
  `ProductName` varchar(55) NOT NULL,
  `ProductID` varchar(55) NOT NULL,
  `ProductDescription` text NOT NULL,
  `Price` double NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Weight` varchar(45) NOT NULL,
  `MfgDate` datetime NOT NULL,
  `ExpiryDate` datetime NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `ModifiedDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartproducts`
--

INSERT INTO `cartproducts` (`CartID`, `ProductName`, `ProductID`, `ProductDescription`, `Price`, `Quantity`, `Weight`, `MfgDate`, `ExpiryDate`, `CreatedDateTime`, `ModifiedDateTime`) VALUES
(100003, 'Machine Gun', '404405104', 'It is to kill people and having fun while using it.', 5000000, 17, '10000', '2017-06-06 00:00:00', '2017-06-08 00:00:00', '2017-10-10 18:11:25', '2017-10-10 19:13:03'),
(100003, 'CD', 'CD', '', 10, 1, '1', '2017-06-06 00:00:00', '2017-06-08 00:00:00', '2017-10-10 17:28:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `CartID` int(11) NOT NULL,
  `CartStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`CartID`, `CartStatus`) VALUES
(100001, 0),
(100002, 0),
(100003, 1),
(100004, 0),
(100005, 0),
(100006, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductName` varchar(55) NOT NULL,
  `ProductID` varchar(55) NOT NULL,
  `ProductDescription` text NOT NULL,
  `Price` double NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Weight` varchar(45) NOT NULL COMMENT 'In Grams',
  `MfgDate` datetime NOT NULL,
  `ExpiryDate` datetime NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `ModifiedDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductName`, `ProductID`, `ProductDescription`, `Price`, `Quantity`, `Weight`, `MfgDate`, `ExpiryDate`, `CreatedDateTime`, `ModifiedDateTime`) VALUES
('Marker', '122465128', 'To write on white board', 10, 1, '15', '2017-06-06 00:00:00', '2017-06-08 00:00:00', '2017-10-10 17:28:51', '0000-00-00 00:00:00'),
('Cup', '146512836', 'Tea Cup', 50, 1, '127', '2017-06-06 00:00:00', '2017-06-08 00:00:00', '2017-10-10 18:11:25', '0000-00-00 00:00:00'),
('', '2c89c6db', '', 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('Vaseline Lip Gaurd', '404405104', 'Helps heal dry lips', 5, 1, '33', '2017-06-06 00:00:00', '2017-06-08 00:00:00', '2017-10-10 18:11:25', '0000-00-00 00:00:00'),
('Spoon', '514040284', 'Metal Spoon', 40, 1, '38', '2017-06-06 00:00:00', '2017-06-08 00:00:00', '2017-10-10 17:28:51', '0000-00-00 00:00:00'),
('', 'feabe42b', '', 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartproducts`
--
ALTER TABLE `cartproducts`
  ADD PRIMARY KEY (`CartID`,`ProductID`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`CartID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `CartID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100007;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
