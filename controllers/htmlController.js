module.exports = function(app, con, config, io) {

    app.get('/carts.php', function(req, res) {
        var base = config.base;
        var cart_id = req.query.cart_id;

        var sel_product = "SELECT CartID as cart_id, CartStatus as cart_status FROM carts";

        con.query(sel_product, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                //res.writeHead(200, { 'Content-Type': 'text/html' });
                res.render('carts', { site_url: config.site_url, data: rows });
            }
        });

    });

    app.get('/cartDetails.php', function(req, res) {
        var cart_id = req.query.cart_id;

        var sel_product = "SELECT CartID as cart_id, ProductName as product_name, ProductID as product_id, ProductDescription as product_desc, Price as price, Quantity as quantity, Weight as weight FROM cartproducts WHERE CartID = '" + cart_id + "'";

        con.query(sel_product, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {

                res.render('cartDetails', { site_url: config.site_url, data: rows, cart: cart_id });
            }
        });
    });

    app.get('/thankyou.php', function(req, res) {

        // Empty Cart
        var cart_id = req.query.cart_id;

        var del_cart = "DELETE FROM cartproducts WHERE CartID = '" + cart_id + "'";

        con.query(del_cart, function(err, result) {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                var update_cart = "UPDATE carts SET CartStatus = 0 WHERE CartID = '" + cart_id + "'";
                con.query(update_cart, function(error, result) {
                    if (error) {
                        console.log("Unable to update the cart status.");
                    } else {
                        console.log("Cart status updated successfully.");
                    }
                });

                io.on('connection', function(socket) {
                    socket.on('message', function() {
                        io.emit('message', "CartIsEmpty");
                    });
                });

            }
        });

        res.render('thankyou');
    });

    app.get('/getCartsStatus.php', function(req, res) {

        var sel_products = "SELECT ProductID as product_id FROM cartproducts GROUP BY cart_id";

        con.query(sel_products, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "SUCCESS", products_list: rows }));
            }
        });
    });

};