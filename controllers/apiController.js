module.exports = function (app, con, config, io) {
    app.post('/createProduct.php', function (req, res) {

        var cart_id = req.body.cart_id;
        var product_name = req.body.product_name;
        var product_id = req.body.product_id;
        var product_desc = req.body.product_desc;
        var price = req.body.price;
        var quantity = 1;
        var weight = req.body.weight; //In Grams
        var mfg_date = "10/12/2017";
        var exp_date = req.body.exp_date;

        var insert_product = "INSERT INTO cartproducts(CartID, ProductName, ProductID, ProductDescription, Price, Quantity, Weight,   CreatedDateTime, ModifiedDateTime)";
        insert_product += " VALUES('" + cart_id + "', '" + product_name + "', '" + product_id + "', '" + product_desc + "', '" + price + "', '" + quantity + "', '" + weight + "', NOW(), NOW())";
        insert_product += " ON DUPLICATE KEY UPDATE Quantity = Quantity + 1, ModifiedDateTime = NOW()";

        con.query(insert_product, function (err, result) {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {

                var update_cart = "UPDATE carts SET CartStatus = 1 WHERE CartID = '" + cart_id + "'";

                con.query(update_cart, function (error, result) {
                    if (error) {
                        console.log("Unable to update the cart status.");
                    } else {
                        console.log("Cart status updated successfully.");
                    }
                });
                if (req.query && req.query.rfid) {
                    console.log("rfid############")
                    io.broadcast.emit('item_scanned', { data: 'world' });
                }

                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'SUCCESS' }));

            }
        });

    });

    app.get('/deleteProduct.php', function (req, res) {
        var cart_id = req.query.cart_id;
        var product_id = req.query.product_id;

        var del_product = "DELETE FROM cartproducts WHERE CartID = '" + cart_id + "' AND ProductID = '" + product_id + "'";

        con.query(del_product, function (err, result) {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {

                io.on('connection', function (socket) {
                    socket.on('message', function () {
                        io.emit('message', "ProductDeleted");
                    });
                });

                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'SUCCESS' }));
            }
        });
    });

    app.get('/emptyCart.php', function (req, res) {
        var cart_id = req.query.cart_id;

        var del_cart = "DELETE FROM cartproducts WHERE CartID = '" + cart_id + "'";

        con.query(del_cart, function (err, result) {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                var update_cart = "UPDATE carts SET CartStatus = 0 WHERE CartID = '" + cart_id + "'";
                con.query(update_cart, function (error, result) {
                    if (error) {
                        console.log("Unable to update the cart status.");
                    } else {
                        console.log("Cart status updated successfully.");
                    }
                });
                if (io) {
                    io.on('connection', function (socket) {
                        socket.on('message', function () {
                            io.emit('message', "CartIsEmpty");
                        });
                    });
                }

                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: 'SUCCESS' }));
            }
        });
    });

    app.get('/getCartInformation.php', function (req, res) {
        var cart_id = req.query.cart_id;

        var sel_product = "SELECT CartID AS cart_id, products.ProductName AS product_name, products.ProductID AS product_id, products.ProductDescription AS product_desc, products.Price AS price, cartproducts.Quantity AS quantity, products.Weight AS weight, products.MfgDate AS mfg_date, products.ExpiryDate AS expiry_date \
        FROM cartproducts \
        INNER JOIN products ON products.ProductID = cartproducts.ProductID \
        WHERE CartID = '" + cart_id + "'";
        console.log(sel_product);

        con.query(sel_product, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {

                var sel_product_count = "SELECT SUM(Quantity*Price) as total_price FROM cartproducts WHERE CartID = '" + cart_id + "'";

                con.query(sel_product_count, (err, total_count_rows) => {
                    if (err) {
                        //throw err;
                        res.writeHead(501, { 'Content-Type': 'application/json' });
                        res.end(JSON.stringify({ message: "FAIL", error: err }));
                    } else {
                        res.writeHead(200, { 'Content-Type': 'application/json' });
                        res.end(JSON.stringify({ message: "SUCCESS", products_list: rows, total_price: total_count_rows[0].total_price }));
                    }
                });
            }
        });
    });

    app.get('/getProductInformation.php', function (req, res) {
        var cart_id = req.query.cart_id;
        var product_id = req.query.product_id;

        var sel_product = "SELECT ProductName as product_name, ProductID as product_id, ProductDescription as product_desc, Price as price, Quantity as quantity, Weight as weight, MfgDate as mfg_date, ExpiryDate as expiry_date";
        sel_product += " FROM products WHERE ProductID = '" + product_id + "'";

        con.query(sel_product, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                if (err) {
                    //throw err;
                    res.writeHead(501, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify({ message: "FAIL", error: err }));
                } else {
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify({ message: "SUCCESS", products_info: rows[0] }));
                }
            }
        });
    });

    app.get('/getActiveCartID.php', function (req, res) {
        /**
         * @clear the cart
         */
        var del_cart = "select * from carts";

        con.query(del_cart, function (err, result) {

            //CartStatus = 0 AND
            var sel_cart_id = "SELECT CartID as cart_id FROM carts WHERE CartID = 100003";

            con.query(sel_cart_id, (err, rows) => {
                if (err) {
                    //throw err;
                    // res.writeHead(501, { 'Content-Type': 'application/json' });
                    // res.end(JSON.stringify({ message: "FAIL", error: err }));
                } else {
                    if (rows.length > 0) {
                        var inactive_cart_id = rows[0]['cart_id'];
                        res.writeHead(200, { 'Content-Type': 'application/json' });
                        res.end(JSON.stringify({ message: "SUCCESS", active_cart: inactive_cart_id }));
                    } else {
                        res.writeHead(200, { 'Content-Type': 'application/json' });
                        res.end(JSON.stringify({ message: "SUCCESS", active_cart: null }));
                    }
                }
            });

        });
        ////// Made the cart as empty


    });

    app.get('/checkCartStatus.php', function (req, res) {
        var cart_id = req.query.cart_id;

        var sel_product = "SELECT COUNT(ProductID) as products_count FROM cartproducts WHERE CartID = '" + cart_id + "'";

        con.query(sel_product, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "SUCCESS", products_count: rows[0].products_count }));
            }
        });
    });

    app.get('/getCartTotalPrice.php', function (req, res) {
        var cart_id = req.query.cart_id;

        var sel_product_count = "SELECT SUM(Quantity*Price) as total_price FROM cartproducts WHERE CartID = '" + cart_id + "'";

        con.query(sel_product_count, (err, rows) => {
            if (err) {
                //throw err;
                res.writeHead(501, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "FAIL", error: err }));
            } else {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ message: "SUCCESS", total_price: rows[0].total_price }));
            }
        });
    });
};