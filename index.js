var express = require('express');
var app = express();
var os = require("os");
var fs = require("fs");
var bodyParser = require('body-parser');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var cons = require('consolidate');

// var io = require('socket.io')(app);
var globalSocket;
io.on('connection', function (socket) {
    console.log("hellow world");
    globalSocket = socket;  
    socket.on('my other event', function (data) {
        console.log(data);
    // socket.emit('item_scanned', { data: 'world' });
          
    });
});



app.use(express.static(__dirname + '/public'));
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

var port = process.env.PORT || 3000;
var config = require("./config.js");

var apiController = require("./controllers/apiController.js");
var htmlController = require("./controllers/htmlController.js");

var mysql = require('mysql');

var con = mysql.createConnection({
    host: config.db.host,
    user: config.db.username,
    password: config.db.password,
    database: config.db.database
});

con.connect((err) => {
    if (err) throw err;
    console.log('Connected!');
});

app.use(bodyParser.urlencoded({
    extended: true
}));



app.use(bodyParser.json());



app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    console.log("Testing#######################");
    if (req.method === 'OPTIONS') {
        return res.send(200);
    } else {
        return next();
    }
});
setTimeout(()=>{
    console.log("API Init done with socket capability");
    // console.log(globalSocket);
    apiController(app, con, config, globalSocket);
    htmlController(app, con, config, globalSocket);

}, 5000)



http.listen(port, function () {
    console.log('listening on *:' + port);
});